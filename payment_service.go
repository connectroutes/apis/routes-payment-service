package main

import (
	"fmt"
	routeModels "gitlab.com/connectroutes/routes-go-lib/models"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"routes-payment-service/models"
	"routes-payment-service/paystack"
	"routes-payment-service/proto"
)

type service struct {
}

func (s service) AddCardByReference(ctx context.Context, request *proto.AddCardByReferenceRequest) (*proto.AddCardResponse, error) {
	user, _ := routeModels.GetUserFromContext(ctx)

	if verified, response, err := paystack.VerifyTransaction(paystack.VerifyTransactionRequest{
		Amount:    1000,
		Reference: request.Reference,
		UserId:    user.UserId,
	}); verified {

		cardAuthorization := response["authorization"].(map[string]interface{})
		id, err := storeCard(user, cardAuthorization, request.Reference)

		if err != nil { //TODO manually check if error is a duplicate error from mongodb.
			return nil, status.Errorf(codes.Internal,
				"Unable to add card")
		}

		return &proto.AddCardResponse{
			Message: "Ok",
			Action:  "success",
			Card:    paymentMethodFromAuthorization(cardAuthorization, id),
		}, nil

	} else if !verified {
		return nil, status.Errorf(codes.InvalidArgument,
			"Cannot Verify Transaction")
	} else if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			err.Error())
	}

	return nil, status.Errorf(codes.Internal,
		"An internal error occurred")
}

func (s service) SubmitCardAuthorization(ctx context.Context, request *proto.SubmitCardAuthorizationRequest) (*proto.AddCardResponse, error) {
	user, _ := routeModels.GetUserFromContext(ctx)

	response, err := paystack.SubmitAuthorization(paystack.SubmitAuthorizationRequest{
		Action:    request.Action,
		Reference: request.Reference,
		Value:     request.Value,
	})

	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument,
			err.Error())
	}

	if response["status"] == "success" {

		cardAuthorization := response["authorization"].(map[string]interface{})
		id, err := storeCard(user, cardAuthorization, request.Reference)

		if err != nil {
			return nil, status.Errorf(codes.Internal,
				"Unable to add card")
		}

		return &proto.AddCardResponse{
			Message: "Ok",
			Action:  "success",
			Card:    paymentMethodFromAuthorization(cardAuthorization, id),
		}, nil

	} else {
		if response["status"].(string) == "open_url" {
			return &proto.AddCardResponse{
				Action:    response["status"].(string),
				ActionUrl: response["url"].(string),
				Reference: response["reference"].(string),
			}, nil
		}
		return &proto.AddCardResponse{
			Message:   response["display_text"].(string),
			Action:    response["status"].(string),
			Reference: response["reference"].(string),
		}, nil
	}

}

func (s service) GetPaymentOptions(ctx context.Context, request *proto.GetPaymentOptionsRequest) (*proto.GetPaymentOptionsResponse, error) {
	user, _ := routeModels.GetUserFromContext(ctx)

	cards, err := models.GetCards(context.Background(), db, user.Id)

	fmt.Println("cards", len(cards))
	if err != nil {
		fmt.Print(err)
		return nil, status.Errorf(codes.Internal,
			"Unable to get cards")
	}

	var options []*proto.PaymentOption

	for _, card := range cards {
		options = append(options, &proto.PaymentOption{
			Method:      proto.PaymentMethod_card,
			CardType:    proto.CardType(card.Type),
			CardLast4:   card.Last4,
			Bin:         card.Bin,
			ExpiryMonth: card.ExpiryMonth,
			ExpiryYear:  card.ExpiryYear,
			Id:          card.Id.Hex(),
		})
	}

	options = append(options, &proto.PaymentOption{
		Method: proto.PaymentMethod_cash,
		Id:     "cash",
	})

	return &proto.GetPaymentOptionsResponse{
		Options: options,
	}, nil
}

func (s service) AddCard(ctx context.Context, request *proto.AddCardRequest) (*proto.AddCardResponse, error) {
	user, _ := routeModels.GetUserFromContext(ctx)

	fmt.Println("add card")
	fmt.Println(user)

	response, err := paystack.ChargeCard(paystack.ChargeCardRequest{
		CardNo:          request.CardNo,
		CardCvv:         request.CardCvv,
		CardExpiryMonth: request.CardExpiryMonth,
		CardExpiryYear:  request.CardExpiryYear,
		Amount:          "1000",
		Email:           "customer-receipts@connectroutes.com",
		Pin:             request.CardPin,
		UserId:          user.UserId,
	})

	if err != nil {
		fmt.Print(err)
		return nil, status.Errorf(codes.InvalidArgument,
			err.Error())
	}

	if response["status"] == "success" {
		cardAuthorization := response["authorization"].(map[string]interface{})
		id, err := storeCard(user, cardAuthorization, response["reference"].(string))

		if err != nil {
			return nil, status.Errorf(codes.Internal,
				"Unable to add card")
		}

		return &proto.AddCardResponse{
			Message: "Ok",
			Action:  "success",
			Card:    paymentMethodFromAuthorization(cardAuthorization, id),
		}, nil

	} else {
		if response["status"].(string) == "open_url" {
			return &proto.AddCardResponse{
				Action:    "open_url",
				ActionUrl: response["url"].(string),
				Reference: response["reference"].(string),
			}, nil
		}
		return &proto.AddCardResponse{
			Message:   response["display_text"].(string),
			Action:    response["status"].(string),
			Reference: response["reference"].(string),
		}, nil
	}
}

func storeCard(user *routeModels.User, cardAuthorization map[string]interface{}, reference string) (string, error) {

	id, err := models.CreateCard(context.Background(), db, bson.M{
		"user_id":      user.Id,
		"type":         getCardType(cardAuthorization["brand"].(string)),
		"last_4":       cardAuthorization["last4"],
		"signature":    cardAuthorization["signature"],
		"bin":          cardAuthorization["bin"],
		"expiry_month": cardAuthorization["exp_month"],
		"expiry_year":  cardAuthorization["exp_year"],
		"active":       true,
		"auth_code":    cardAuthorization["authorization_code"],
		"reference":    reference,
	})

	return id, err
}

func paymentMethodFromAuthorization(cardAuthorization map[string]interface{}, cardId string) *proto.PaymentOption {
	return &proto.PaymentOption{
		Method:      proto.PaymentMethod_card,
		Id:          cardId,
		CardType:    proto.CardType(getCardType(cardAuthorization["brand"].(string))),
		CardLast4:   cardAuthorization["last4"].(string),
		Bin:         cardAuthorization["bin"].(string),
		ExpiryMonth: cardAuthorization["exp_month"].(string),
		ExpiryYear:  cardAuthorization["exp_year"].(string),
	}
}
func getCardType(brand string) int {
	if brand == "visa" {
		return 0
	}
	return 1

}
