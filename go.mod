module routes-payment-service

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	github.com/sergi/go-diff v1.1.0 // indirect
	gitlab.com/connectroutes/routes-go-lib v0.0.0-20200308080130-5cc7d5f0aaa4
	go.mongodb.org/mongo-driver v1.3.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/grpc v1.27.1
)
