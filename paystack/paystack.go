package paystack

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

var paystackBaseUrl = "https://api.paystack.co"

type ChargeCardRequest struct {
	CardNo          string
	CardCvv         string
	CardExpiryMonth string
	CardExpiryYear  string
	Amount          string
	Email           string
	Pin             string
	UserId          string
}

type ChargeCardResponse struct {
	Reference   string
	Status      string
	DisplayText string `json:"display_text"`
}

type SubmitAuthorizationRequest struct {
	Action    string
	Reference string
	Value     string
}

type VerifyTransactionRequest struct {
	Amount    float64
	Reference string
	UserId    string
}

func VerifyTransaction(verifyTransactionRequest VerifyTransactionRequest) (bool, map[string]interface{}, error) {

	responseBody, statusCode, err := getRequest("/transaction/verify/" + verifyTransactionRequest.Reference)

	fmt.Println("status code", statusCode)

	if err != nil {
		fmt.Println(err)
		return false, map[string]interface{}{}, errors.New("internal error")
	}

	fmt.Println("response Body:", string(responseBody))

	if statusCode != 200 && len(responseBody) != 0 {
		var errorResponse map[string]interface{}
		json.Unmarshal(responseBody, &errorResponse)
		return false, map[string]interface{}{}, errors.New(errorResponse["message"].(string))
	}

	var paystackResponse map[string]interface{}

	json.Unmarshal(responseBody, &paystackResponse)

	data := paystackResponse["data"].(map[string]interface{})

	return data["status"].(string) == "success" && data["metadata"].(map[string]interface{})["user_id"] == verifyTransactionRequest.UserId && data["amount"].(float64) >= verifyTransactionRequest.Amount, paystackResponse["data"].(map[string]interface{}), nil

}
func SubmitAuthorization(submitAuthorizationRequest SubmitAuthorizationRequest) (map[string]interface{}, error) {
	authorizationType := strings.Split(submitAuthorizationRequest.Action, "_")[1] //send_otp, send_pin. Extract otp/pin

	body := map[string]string{
		authorizationType: submitAuthorizationRequest.Value,
		"reference":       submitAuthorizationRequest.Reference,
	}

	fmt.Println(body)
	responseBody, statusCode, err := runRequest(body, "/charge/"+strings.Replace(submitAuthorizationRequest.Action, "send", "submit", 1)) // replace send_otp, send_pin with submit_otp, submit_pin

	fmt.Println("status code", statusCode)

	if err != nil {
		fmt.Println(err)
		return map[string]interface{}{}, errors.New("internal error")
	}

	fmt.Println("response Body:", string(responseBody))

	if statusCode != 200 {
		if len(responseBody) != 0 {
			var errorResponse map[string]interface{}
			json.Unmarshal(responseBody, &errorResponse)
			return map[string]interface{}{}, errors.New(errorResponse["message"].(string))
		} else {
			return map[string]interface{}{}, errors.New("an unknown error occurred")
		}

	}

	var paystackResponse map[string]interface{}

	json.Unmarshal(responseBody, &paystackResponse)

	return paystackResponse["data"].(map[string]interface{}), nil

}

func ChargeCard(chargeCardRequest ChargeCardRequest) (map[string]interface{}, error) {
	body := map[string]interface{}{
		"email":  chargeCardRequest.Email,  //TODO env
		"amount": chargeCardRequest.Amount, //authorization charge
		"card": map[string]string{
			"cvv":          chargeCardRequest.CardCvv,
			"number":       chargeCardRequest.CardNo,
			"expiry_month": chargeCardRequest.CardExpiryMonth,
			"expiry_year":  chargeCardRequest.CardExpiryYear,
		},
		"pin": chargeCardRequest.Pin,
		"metadata": map[string]string{
			"user_id": chargeCardRequest.UserId,
		},
	}

	responseBody, statusCode, err := runRequest(body, "/charge")

	if err != nil {
		fmt.Println(err)
		return map[string]interface{}{}, errors.New("internal error")
	}

	fmt.Println("response Body:", string(responseBody))

	if statusCode != 200 {
		var errorResponse map[string]interface{}
		json.Unmarshal(responseBody, &errorResponse)
		return map[string]interface{}{}, errors.New(errorResponse["message"].(string))

	}

	var paystackResponse map[string]interface{}

	json.Unmarshal(responseBody, &paystackResponse)

	return paystackResponse["data"].(map[string]interface{}), nil

}

func getRequest(path string) ([]byte, int, error) {
	fmt.Println("calling url " + paystackBaseUrl + path)

	httpRequest, _ := http.NewRequest("GET", paystackBaseUrl+path, nil)
	httpRequest.Header.Set("Authorization", "Bearer "+os.Getenv("PAYSTACK_API_KEY"))

	client := &http.Client{}

	resp, err := client.Do(httpRequest)

	if err != nil {
		return nil, 0, err
	}

	defer resp.Body.Close()

	responseBody, err := ioutil.ReadAll(resp.Body)

	return responseBody, resp.StatusCode, err

}

func runRequest(body interface{}, path string) ([]byte, int, error) {
	jsonString, _ := json.Marshal(body)

	fmt.Println("calling url " + paystackBaseUrl + path)

	httpRequest, _ := http.NewRequest("POST", paystackBaseUrl+path, bytes.NewBuffer(jsonString))
	httpRequest.Header.Set("Authorization", "Bearer "+os.Getenv("PAYSTACK_API_KEY"))
	httpRequest.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(httpRequest)

	if err != nil {
		return nil, 0, err
	}

	defer resp.Body.Close()

	responseBody, err := ioutil.ReadAll(resp.Body)

	return responseBody, resp.StatusCode, err

}
