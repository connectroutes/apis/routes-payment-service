package main

import (
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"google.golang.org/grpc"
	"log"
	"net"
	"routes-payment-service/proto"
)

func main(){
	lis, err := net.Listen("tcp", "0.0.0.0:50055")

	if err != nil {
		log.Fatalf("Could not listen on port: %v", err)
	}
	fmt.Println("Server Started")

	server := grpc.NewServer()

	proto.RegisterPaymentServiceServer(server, &service{})

	if err := server.Serve(lis); err != nil {
		log.Fatal(err)
	}
}