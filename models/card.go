package models

import (
	"fmt"
	"gitlab.com/connectroutes/routes-go-lib/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"time"
)

type Card struct {
	Id                primitive.ObjectID `bson:"_id"`
	Type              int32              `bson:"type" json:"type"`
	Active            bool               `bson:"active" json:"active"`
	Last4             string             `bson:"last_4" json:"last_4"`
	Bin               string             `json:"bin"`
	ExpiryMonth       string             `bson:"expiry_month" json:"expiry_month"`
	ExpiryYear        string             `bson:"expiry_year" json:"expiry_year"`
	Bank              string             `bson:"bank" json:"bank"`
	UserId            primitive.ObjectID `bson:"user_id" json:"user_id"`
	AuthorizationCode string             `bson:"auth_code" json:"auth_code"`
	LastUsed          time.Time          `bson:"last_used" json:"last_used"`
	CreatedAt         time.Time          `bson:"createdAt" json:"created_at"`
	UpdatedAt         time.Time          `bson:"updatedAt" json:"updated_at"`
}

var collectionName = "cards"

func GetCards(ctx context.Context, db *mongo.Database, userId primitive.ObjectID) ([]*Card, error) {

	fmt.Println("getting cards")
	fmt.Println(userId)

	var cards []*Card

	cursor, err := db.Collection(collectionName).Find(ctx, bson.M{"user_id": userId, "active": true})

	defer cursor.Close(ctx)

	if err != nil {
		return nil, err
	}

	for cursor.Next(ctx) {
		fmt.Println("has next")
		var card *Card
		err := cursor.Decode(&card)
		if err != nil {
			return nil, err
		}
		cards = append(cards, card)
	}

	return cards, nil
}

func CreateCard(ctx context.Context, db *mongo.Database, document bson.M) (string, error) {
	return models.CreateDocument(ctx, db, collectionName, document)
}
